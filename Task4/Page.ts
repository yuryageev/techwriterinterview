namespace MyLib {
	/**
	 * Base class for pages.
	 * Page is one of the base building blocks used to create web application.
	 * It represents UI content that can be navigated by user.
	 */
	export abstract class Page {
		get parameters(): Record<string, any> {
			return this._parameters;
		}

		constructor(protected _parameters: Record<string, any>) {
		}

		/**
		 * A callback handler to call as soon as the page has been mounted to the DOM.
		 * Should be used to add page content.
		 * @param el DOM element where page is mounted.
		 */
		abstract onMount(el: Element): void;
	}
}