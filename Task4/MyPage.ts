namespace App {
	export class MyPage extends MyLib.Page {
		constructor(protected _parameters: Record<string, any>) {
			super(_parameters);
		}

		onMount(el: Element): void {
			//page is mounted, ready to add and render page content
			//use this._parameters to access input parameters passed to the page
		}
	}
}