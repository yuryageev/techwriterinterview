namespace MyLib {
    export interface IComponent {
        readonly id: string;
        dispose: () => void;
    }

    export enum ButtonSize {
        Small,
        Medium,
        Large
    }

    export interface IButtonComponent extends IComponent {
        /**
		 * Gets or sets the text of the button.
		 *
		 * @type {string}
		 * @memberof IButtonComponent
		 */
		text: string;

        /**
		 * Gets or sets the size of the button.
		 *
		 * @type {ButtonSize}
		 * @memberof IButtonComponent
		 */
        size: ButtonSize;

		/**
		 * Gets or sets whether the button is enabled or not.
		 *
		 * @type {boolean}
		 * @memberof IButtonComponent
		 */
         enabled: boolean;

		/**
		 * Executes the action of the button.
		 *
		 * @type {Function}
		 * @memberof IButtonComponent
		 */
		click: () => void;
	}
}

function getButton(id: string): MyLib.IButtonComponent | null {
	//instance of button component with corresponding id
	return null;
}

//Usage examples
const button = getButton("button1");

//examples: 
//1. how to change button's size?
//2. how to get button's text? 
//3. how to disable button?
//4. how to click on button?
