1. Based on given JSON Schema defined in the user.schema.json file write reference documentation in markdown
2. Provide an example of valid JSON object in the file user1.json
3. Validate your example by enabling JSON Schema validation
    - Go to the .vscode/settings.json and uncomment JSON Schema mapping for Task 2
4. Insert the example as code block into markdown document